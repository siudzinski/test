namespace Test.Settings
{
    public class FeatureSettings
    {
        public bool FeatureFooIsEnabled { get; set; }
        public bool FeatureBarIsEnabled { get; set; }
    }
}