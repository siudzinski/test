using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Test.Authorization;
using Test.Authorization.FeatureBar;
using Test.Filters.Attributes;

namespace Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [FeatureBarEnabled]
    public class FeatureBarController
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "bar1", "bar2" };
        }
    }
}