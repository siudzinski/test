using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Test.Authorization.FeatureFoo;
using Test.Filters.Attributes;

namespace Test.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [FeatureFooEnabled]
    public class FeatureFooController
    {
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "foo1", "foo2" };
        }
    }
}