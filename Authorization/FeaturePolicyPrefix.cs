namespace Test.Authorization
{
    public static class FeaturePolicyPrefix
    {
        public static string FeatureFoo { get { return "FeatureFoo";} } 
        public static string FeatureBar { get { return "FeatureBar";} }
    }
}