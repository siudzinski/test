using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Test.Settings;

namespace Test.Authorization.FeatureFoo
{
    public class FeatureFooAuthorizationHandler : AuthorizationHandler<FeatureFooIsEnabledRequirement>
    {
        private readonly FeatureSettings _featureSettings;

        public FeatureFooAuthorizationHandler(FeatureSettings featureSettings)
        {
            _featureSettings = featureSettings;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, FeatureFooIsEnabledRequirement requirement)
        {
            if(_featureSettings.FeatureFooIsEnabled)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            throw new FeatureAuthorizationException("FeatureFoo");
        }
    }
}