using Microsoft.AspNetCore.Authorization;

namespace Test.Authorization.FeatureFoo
{
    public class FeatureFooAuthorize : AuthorizeAttribute
    {
        public FeatureFooAuthorize()  
        {
            Policy = FeaturePolicyPrefix.FeatureFoo;
        }
    }
}