using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Test.Authorization.FeatureBar;
using Test.Authorization.FeatureFoo;

namespace Test.Authorization
{
    public class FeaturePolicyProvider : IAuthorizationPolicyProvider
    {
        private IDictionary<string, IAuthorizationRequirement> _requirementsByPolicyName;

        public DefaultAuthorizationPolicyProvider FallbackPolicyProvider { get; }

        public FeaturePolicyProvider(IOptions<AuthorizationOptions> options)
        {
            _requirementsByPolicyName = new Dictionary<string, IAuthorizationRequirement>
            {
                { FeaturePolicyPrefix.FeatureFoo, new FeatureFooIsEnabledRequirement() },
                { FeaturePolicyPrefix.FeatureBar, new FeatureBarIsEnabledRequirement() }
            };

            // ASP.NET Core only uses one authorization policy provider, so if the custom implementation
            // doesn't handle all policies (including default policies, etc.) it should fall back to an
            // alternate provider.
            //
            // In this sample, a default authorization policy provider (constructed with options from the 
            // dependency injection container) is used if this custom provider isn't able to handle a given
            // policy name.
            //
            // If a custom policy provider is able to handle all expected policy names then, of course, this
            // fallback pattern is unnecessary.
            FallbackPolicyProvider = new DefaultAuthorizationPolicyProvider(options);
        }

        public Task<AuthorizationPolicy> GetDefaultPolicyAsync() => FallbackPolicyProvider.GetDefaultPolicyAsync();

        public Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {
            if (_requirementsByPolicyName.ContainsKey(policyName))
            {
                var policy = new AuthorizationPolicyBuilder();
                policy.AddRequirements(_requirementsByPolicyName[policyName]);
                return Task.FromResult(policy.Build());
            }

            // If the policy name doesn't match the format expected by this policy provider,
            // try the fallback provider. If no fallback provider is used, this would return 
            // Task.FromResult<AuthorizationPolicy>(null) instead.
            return FallbackPolicyProvider.GetPolicyAsync(policyName);
        }
    }
}