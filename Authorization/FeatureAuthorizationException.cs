using System;

namespace Test.Authorization
{
    public class FeatureAuthorizationException : Exception
    {
        public FeatureAuthorizationException(string featureName) 
            : base($"Authorization failure for {featureName} feature.")
        {

        }
    }
}