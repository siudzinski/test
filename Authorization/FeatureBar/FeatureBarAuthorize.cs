using Microsoft.AspNetCore.Authorization;

namespace Test.Authorization.FeatureBar
{
    public class FeatureBarAuthorize : AuthorizeAttribute
    {
        public FeatureBarAuthorize()  
        {
            Policy = FeaturePolicyPrefix.FeatureBar;
        }
    }
}