using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using Test.Settings;

namespace Test.Authorization.FeatureBar
{
    public class FeatureBarAuthorizationHandler : AuthorizationHandler<FeatureBarIsEnabledRequirement>
    {
        private readonly FeatureSettings _featureSettings;

        public FeatureBarAuthorizationHandler(FeatureSettings featureSettings)
        {
            _featureSettings = featureSettings;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, FeatureBarIsEnabledRequirement requirement)
        {
            if(_featureSettings.FeatureBarIsEnabled)
            {
                context.Succeed(requirement);
                return Task.CompletedTask;
            }

            throw new FeatureAuthorizationException("FeatureBar");
        }
    }
}