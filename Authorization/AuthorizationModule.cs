using Autofac;
using Test.Authorization.FeatureBar;
using Test.Authorization.FeatureFoo;

namespace Test.Authorization
{
    public class AuthorizationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<FeaturePolicyProvider>().AsImplementedInterfaces().SingleInstance();
            builder.RegisterType<FeatureFooAuthorizationHandler>().AsImplementedInterfaces();
            builder.RegisterType<FeatureBarAuthorizationHandler>().AsImplementedInterfaces();
        }
    }
}