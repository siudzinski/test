using System;

namespace Test.Filters.Attributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class FeatureFooEnabledAttribute : Attribute, IFeatureAttribute
    {
        
    }
}