using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Test.Filters.Attributes;
using Test.Settings;

namespace Test.Filters
{
    public class FeatureSwitchFilter : IAsyncActionFilter
    {
        private readonly FeatureSettings _featureSettings;
        private readonly IDictionary<Type, bool> _enabledFeatures;

        public FeatureSwitchFilter(FeatureSettings featureSettings)
        {
            _featureSettings = featureSettings;
            _enabledFeatures = new Dictionary<Type, bool>
            {
                { typeof(FeatureFooEnabledAttribute), _featureSettings.FeatureFooIsEnabled },
                { typeof(FeatureBarEnabledAttribute), _featureSettings.FeatureBarIsEnabled }
            };
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var descriptor = context.ActionDescriptor as ControllerActionDescriptor;
            if (descriptor != null)
            {
                var attribute = descriptor.ControllerTypeInfo
                    .GetCustomAttributes(inherit: true)
                    .FirstOrDefault(a => a.GetType().GetInterfaces().Contains(typeof(IFeatureAttribute)));

                if(attribute != null)
                {
                    var attributeType = attribute.GetType();
                    if(_enabledFeatures.ContainsKey(attributeType) && !_enabledFeatures[attributeType])
                    {
                        throw new FeatureIsDisabledException();
                    }
                }
            }
            await next();
        }
    }
}