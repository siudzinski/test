using System;

namespace Test.Filters
{
    public class FeatureIsDisabledException : Exception
    {
        public FeatureIsDisabledException() 
            : base($"Feature is disabled.")
        {

        }
    }
}